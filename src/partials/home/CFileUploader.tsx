import { Icon, message, Upload } from "antd";
import {
  RcFile,
  UploadChangeParam,
  UploadFile,
  UploadProps
} from "antd/lib/upload/interface";
import axios, { AxiosResponse } from "axios";
import { inject, observer } from "mobx-react";
import React, { Component } from "react";

import { IStores } from "../../CApp";
import CWSUploader from "../../utils/CWSUploader";

interface CFileUploaderProps {
  stores?: IStores;
}

@inject("stores")
@observer
export default class CFileUploader extends Component<CFileUploaderProps> {
  private readonly store = this.props.stores!.file;

  private readonly wsUploader: CWSUploader = new CWSUploader(
    "wss://api.put.re/upload_ws"
  );

  private readonly options: UploadProps = {
    action: "https://api.put.re/upload",
    beforeUpload: (file: RcFile) => this.beforeUpload(file),
    customRequest: options => this.customRequest(options),
    multiple: true,
    name: "file",
    onChange: (info: UploadChangeParam<UploadFile>) => this.onChange(info),
    showUploadList: false
  };

  private readonly limits: { http: number; max: number } = {
    http: 50 * 1024 * 1024,
    max: 200 * 1024 * 1024
  };

  private beforeUpload(file: RcFile): boolean {
    if (file.size >= this.limits.max) {
      message.error(`File is bigger than ${this.limits.max / (1024 * 1024)}MB`);
      return false;
    }

    return true;
  }

  // TODO Types
  private customRequest(options: any) {
    if (options.file.size > this.limits.http) {
      return this.wsRequest(options);
    }

    return this.httpRequest(options);
  }

  private async httpRequest(options: any) {
    const {
      action,
      data,
      file,
      filename,
      headers,
      onError,
      onProgress,
      onSuccess,
      withCredentials
    } = options;

    const formData = new FormData();

    if (data) {
      Object.keys(data).forEach(key => {
        formData.append(key, data[key]);
      });
    }

    formData.append(filename, file);

    try {
      const response: AxiosResponse<APIUploadResponse> = await axios.post(
        action,
        formData,
        {
          withCredentials,
          headers,
          onUploadProgress: ({ total, loaded }) =>
            onProgress({ percent: (loaded / total) * 100 }, file)
        }
      );

      onSuccess(response.data, file);
    } catch (_) {
      onError();
    }

    return {
      abort() {}
    };
  }

  private wsRequest(options: any) {
    const { onError, onProgress, onSuccess } = options;

    const rcFile: RcFile = options.file;

    this.wsUploader.on("error", (file: RcFile) => {
      if (file.uid !== rcFile.uid) {
        return;
      }

      onError();
    });

    this.wsUploader.on("progress", (file: RcFile, progress: number) => {
      if (file.uid !== rcFile.uid) {
        return;
      }

      onProgress({ percent: progress }, file);
    });

    this.wsUploader.on(
      "finish",
      (file: RcFile, response: APIUploadResponse) => {
        if (file.uid !== rcFile.uid) {
          return;
        }

        onSuccess({ data: response }, file);
      }
    );

    this.wsUploader.queueFile(rcFile);
  }

  private onChange(info: UploadChangeParam<MyUploadFile>): void {
    this.store.upsertEntry(info.file);
  }

  render() {
    return (
      <Upload.Dragger {...this.options}>
        <p className="ant-upload-drag-icon">
          <Icon style={{ fontSize: "64px" }} type="cloud-upload" />
        </p>
        <p className="ant-upload-text">
          Click or drag files to this area to upload them
        </p>
        <p className="ant-upload-hint">
          Make sure to read our terms of service before uploading files
        </p>
      </Upload.Dragger>
    );
  }
}
