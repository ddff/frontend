import { Button, Progress, Spin, Table, Typography } from "antd";
import { ColumnProps } from "antd/es/table";
import { inject, observer, Observer } from "mobx-react";
import React, { Component } from "react";
import MediaQuery from "react-responsive";

import { IStores } from "../../CApp";

interface CFileListProps {
  stores?: IStores;
}

@inject("stores")
@observer
export default class CFileList extends Component<CFileListProps> {
  private stores = this.props.stores!;

  private get mobileColumns(): ColumnProps<MyUploadFile>[] {
    return [
      {
        key: "file_name",
        dataIndex: "file_name",
        title: "File",
        render: (_, record: MyUploadFile) => (
          <Typography.Text>{record.name}</Typography.Text>
        )
      },
      {
        key: "file_url",
        dataIndex: "file_url",
        title: "URL",
        render: (_, record: MyUploadFile) => this.formatUrl(record, "link")
      }
    ];
  }

  private get desktopColumns(): ColumnProps<MyUploadFile>[] {
    return [
      {
        key: "file_name",
        dataIndex: "file_name",
        title: "File",
        render: (_, record: MyUploadFile) => (
          <Typography.Text>{record.name}</Typography.Text>
        )
      },
      {
        key: "file_url",
        dataIndex: "file_url",
        title: "URL",
        render: (_, record: MyUploadFile) => this.formatUrl(record, "link")
      },
      {
        key: "deletion_url",
        dataIndex: "deletion_url",
        title: "Deletion",
        render: (_, record: MyUploadFile) =>
          this.formatUrl(record, "deleteLink")
      },
      {
        key: "progress",
        dataIndex: "progress",
        title: "Progress",
        render: (_, record: MyUploadFile) => (
          <Progress
            percent={Math.round(record.percent!)}
            size="small"
            status={record.status === "done" ? "success" : "active"}
          />
        )
      },
      {
        title: "Action",
        key: "action",
        render: (_, record: MyUploadFile) => (
          <Observer>
            {() => {
              const canEdit = this.stores.album.canEdit;

              // TODO Refactor this mess
              const link =
                record.response &&
                record.response.data &&
                record.response.data.link.split("/").pop();

              const isPresent = link
                ? this.stores.album.album.entries.some(
                    value => value.fileId === link
                  )
                : false;
              // Up to here

              return (
                <Button
                  disabled={isPresent}
                  type="link"
                  onClick={() => {
                    if (canEdit) {
                      this.stores.album.addEntryByFile(record);
                    } else {
                      this.stores.file.toggleModal(0);
                    }
                  }}
                >
                  {canEdit
                    ? isPresent
                      ? "Added to album"
                      : "Add to album"
                    : "Configure album"}
                </Button>
              );
            }}
          </Observer>
        )
      }
    ];
  }

  private formatUrl(
    record: MyUploadFile,
    key: "deleteLink" | "link"
  ): JSX.Element {
    if (!record.response) {
      return <Spin />;
    }
    let url = record.response!.data![key];

    if (
      key === "link" &&
      [".mp4", ".webm"].filter(value => url.toLowerCase().endsWith(value))
        .length > 0
    ) {
      url = `https://put.re/player/${url.split("/").pop()}`;
    }

    const finished = record.status === "done";

    return (
      <Typography.Text copyable={finished} disabled={!finished}>
        {url}
      </Typography.Text>
    );
  }

  render() {
    return (
      <MediaQuery maxWidth={768}>
        {match => (
          <Observer>
            {() => (
              <Table<MyUploadFile>
                columns={match ? this.mobileColumns : this.desktopColumns}
                dataSource={this.stores.file.files.slice()}
                rowKey={record => record.uid}
              />
            )}
          </Observer>
        )}
      </MediaQuery>
    );
  }
}
