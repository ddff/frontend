import { Button, Col, Form, Input, message, Row } from "antd";
import { FormComponentProps } from "antd/es/form";
import axios, { AxiosResponse } from "axios";
import { inject, observer } from "mobx-react";
import React, { Component } from "react";

import { IStores } from "../../CApp";
import spreadFile from "../../utils/spreadFile";

interface CTextUploaderProps extends FormComponentProps {
  body?: string;
  stores?: IStores;
  title?: string;
}

@inject("stores")
@observer
class CTextUploader extends Component<CTextUploaderProps> {
  private readonly store = this.props.stores!.file;

  private ensureExtension(title: string): string {
    return title.split(".").length > 1 ? title : `${title}.txt`;
  }

  private handleSubmit = async (
    e: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    e.preventDefault();

    this.props.form.validateFields(async (err, values) => {
      if (err) {
        return;
      }

      const title = values.title
        ? this.ensureExtension(values.title)
        : "pasted-text.txt";

      const file = new File(
        [new Blob([values.body], { type: "text/plain" })],
        title
      );

      const formData = new FormData();
      formData.append("file", file);

      try {
        const response: AxiosResponse<APIUploadResponse> = await axios.post(
          "https://api.put.re/upload",
          formData,
          {
            headers: {
              "Content-Type": "multipart/form-data"
            }
          }
        );

        this.store.upsertEntry(
          spreadFile(file, {
            percent: 100,
            response: response.data,
            status: "done"
          })
        );
      } catch (_) {
        message.error("Request failed! Please try again later.");
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit}>
        <Row justify="end" type="flex">
          <Col>
            <Form.Item>
              {getFieldDecorator("title", {
                initialValue: this.props.title
              })(<Input placeholder="pasted-text.txt" />)}{" "}
            </Form.Item>
          </Col>

          <Col style={{ padding: "0px 0px 0px 5px" }}>
            <Form.Item>
              <Button htmlType="submit" type="primary">
                Submit
              </Button>
            </Form.Item>
          </Col>
        </Row>

        <Row>
          <Col>
            <Form.Item>
              {getFieldDecorator("body", {
                initialValue: this.props.body,
                rules: [
                  {
                    required: true,
                    message: "We do not accept empty uploads"
                  }
                ]
              })(<Input.TextArea autosize={{ minRows: 16, maxRows: 32 }} />)}
            </Form.Item>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default Form.create<CTextUploaderProps>({ name: "text-uploader" })(
  CTextUploader
);
