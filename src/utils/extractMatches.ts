import axios from "axios";

interface FileExtract {
  fileId: string;
  metadata: APIMetadataBody;
}

const regex = /\w{5,8}\.\w*\.?\w*/gm;

export default async (text: string): Promise<FileExtract[]> => {
  let matches: RegExpExecArray | null;
  let result: FileExtract[] = [];

  while ((matches = regex.exec(text)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (matches.index === regex.lastIndex) {
      regex.lastIndex++;
    }

    for (const match of matches) {
      try {
        const { data } = await axios.get(
          `https://api.put.re/metadata/${match}`
        );

        result.push({ fileId: match, metadata: data.data });
      } catch (_) {}
    }
  }

  return result;
};
