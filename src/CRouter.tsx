/* eslint-disable */

import { Spin } from "antd";
import React, { Component, lazy, Suspense } from "react";
import { Route, Switch } from "react-router-dom";

const CAlbum = lazy(() =>
  import(/* webpackPrefetch: true */ "./routes/CAlbum")
);
const CAPI = lazy(() => import("./routes/CAPI"));
const CFeatures = lazy(() => import("./routes/CFeatures"));
const CHome = lazy(() => import(/* webpackPreload: true */ "./routes/CHome"));
const CNotFound = lazy(() => import("./routes/CNotFound"));
const CPlayer = lazy(() => import("./routes/CPlayer"));
const CPrivacy = lazy(() => import("./routes/CPrivacy"));
const CRemoval = lazy(() => import("./routes/CRemoval"));
const CStatus = lazy(() => import("./routes/CStatus"));
const CTerms = lazy(() => import("./routes/CTerms"));

let CPurgeCSS: React.LazyExoticComponent<any> | undefined;
if (process.env.REACT_APP_PURGECSS || process.env.NODE_ENV !== "production") {
  CPurgeCSS = lazy(() => import("./routes/CPurgeCSS"));
}

export default class CRouter extends Component {
  render() {
    return (
      <>
        <Suspense
          fallback={
            <Spin
              size="large"
              style={{ position: "relative", top: "50%", left: "50%" }}
            />
          }
        >
          <Switch>
            <Route path="/" component={CHome} exact />
            <Route path="/album/:albumId?/:secret?" component={CAlbum} />
            <Route path="/api-docs" component={CAPI} />
            <Route path="/features" component={CFeatures} />
            <Route path="/player/:videoId?" component={CPlayer} />
            <Route path="/privacy" component={CPrivacy} />
            {CPurgeCSS && <Route path="/purgecss" component={CPurgeCSS} />}
            <Route path="/removal-request" component={CRemoval} />
            <Route path="/status" component={CStatus} />
            <Route path="/terms-of-service" component={CTerms} />
            <Route component={CNotFound} />
          </Switch>
        </Suspense>
      </>
    );
  }
}
