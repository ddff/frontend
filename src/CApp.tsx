import "./CApp.css";

import { Col, Layout, Row } from "antd";
import { Observer, Provider } from "mobx-react";
import React, { Component } from "react";
import Headroom from "react-headroom";
import MediaQuery from "react-responsive";
import { BrowserRouter as Router } from "react-router-dom";

import CRouter from "./CRouter";
import CMenu from "./partials/app/CMenu";
import CAlbumStoreInstance, { CAlbumStore } from "./stores/CAlbumStore";
import CFileStoreInstance, { CFileStore } from "./stores/CFileStore";

export interface IStores {
  album: CAlbumStore;
  file: CFileStore;
}

export default class CApp extends Component {
  private stores: IStores = {
    album: CAlbumStoreInstance,
    file: CFileStoreInstance
  };

  render() {
    return (
      <>
        <Provider stores={this.stores}>
          <Router>
            <Layout style={{ minHeight: "100vh" }}>
              <Headroom style={{ padding: "2vh 0vw 0vh 0vw", zIndex: 5 }}>
                <Layout.Header
                  style={{
                    background: "initial"
                  }}
                >
                  <Row justify="center" type="flex">
                    <Col lg={18} md={20} sm={22} xl={16} xs={24}>
                      <MediaQuery maxWidth={768}>
                        {match => (
                          <Observer>{() => <CMenu mobile={match} />}</Observer>
                        )}
                      </MediaQuery>
                    </Col>
                  </Row>
                </Layout.Header>
              </Headroom>

              <Layout.Content style={{ padding: "7vh 0vw 0vh 0vw" }}>
                <Row justify="center" type="flex">
                  <Col lg={16} md={18} sm={20} xl={14} xs={22}>
                    <CRouter />
                  </Col>
                </Row>
              </Layout.Content>

              <Layout.Footer>
                <Row justify="center" type="flex">
                  <Col>
                    <a
                      href="https://gitlab.com/putre/frontend"
                      rel="noopener noreferrer"
                      target="_blank"
                    >
                      {process.env.REACT_APP_GIT_SHA}
                    </a>
                  </Col>
                </Row>
              </Layout.Footer>
            </Layout>
          </Router>
        </Provider>
      </>
    );
  }
}
