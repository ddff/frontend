import { Card, Col, Divider, Row, Tabs, Typography } from "antd";
import React, { PureComponent } from "react";

import CCode from "../partials/app/CCode";

const { Title, Paragraph } = Typography;

const file_upload = JSON.stringify(
  {
    status: "success",
    data: {
      originalName: "pratum.png",
      name: "fdmYuywt.png",
      extension: ".jpg",
      deleteToken: "uAfZhxyK",
      size: 56262,
      thumbnailLink: "https://s.put.re/thumb/fdmYuywt.png",
      link: "https://s.put.re/fdmYuywt.png",
      deleteLink: "https://api.put.re/delete/fdmYuywt.png/uAfZhxyK"
    }
  },
  null,
  2
);

const file_receive =
  '...\nout GET /fdmYuywt.png\nHTTP/1.1\nout Host: s.put.re\nin Content-Type: image/png\nin Content-Length: 23801\nin Content-Disposition: inline; filename="pratum.png"\n...';

const file_delete = JSON.stringify(
  {
    status: "success",
    data: {}
  },
  null,
  2
);

const file_metadata = JSON.stringify(
  {
    status: "success",
    data: {
      size: 56262,
      mimeType: "image/png",
      originalName: "pratum.png"
    }
  },
  null,
  2
);

const album_create = JSON.stringify(
  {
    status: "success",
    data: {
      id: "cZEWUaHB",
      secret: "dPczezj5"
    }
  },
  null,
  2
);

const album_receive = JSON.stringify(
  {
    status: "success",
    data: {
      title: "",
      description: "",
      createdAt: 1568321225038,
      updatedAt: 1568321225038,
      entries: [
        {
          createdAt: 1568321489673,
          description: "",
          dimensions: {
            height: 192,
            width: 192
          },
          fileId: "fdmYuywt.png",
          title: "pratum.png",
          updatedAt: 1568321489673
        }
      ]
    }
  },
  null,
  2
);

const album_modify = JSON.stringify(
  {
    status: "success",
    message: "Album modified"
  },
  null,
  2
);

const album_delete = JSON.stringify(
  {
    status: "success",
    message: "Album deleted"
  },
  null,
  2
);

const misc_health = JSON.stringify(
  {
    status: "success",
    data: {
      uptime: 15721146,
      load: [0.36962890625, 0.4208984375, 0.376953125],
      services: [
        {
          name: "MySQL",
          status: "OK",
          ping: 13
        },
        {
          name: "Amazon AWS S3",
          status: "OK",
          ping: 50
        }
      ]
    }
  },
  null,
  2
);

const misc_removal_req = `curl https://api.put.re/removal \\ \n\t-H 'Content-Type: application/json' \\ \n\t--data '{"files": "https://s.put.re/fdmYuywt.png, https://s.put.re/TAbjrk.jpg","reason": "A valid reason here"}'`;

const misc_removal_res = JSON.stringify(
  {
    status: "success"
  },
  null,
  2
);

interface CAPIProps {
  activePage?: string;
}

export default class CAPI extends PureComponent<CAPIProps> {
  render() {
    return (
      <>
        <Row>
          <Col>
            <Title>API</Title>
          </Col>
        </Row>

        <Row>
          <Col>
            <Divider />
          </Col>
        </Row>

        <Tabs animated={false} defaultActiveKey={this.props.activePage || "1"}>
          <Tabs.TabPane key="1" tab="Files">
            <Row>
              <Col>
                <Card>
                  <Title level={2}>Upload a file</Title>
                  <Title level={3}>Request</Title>
                  <Paragraph code>
                    curl -F file=@pratum.png https://api.put.re/upload
                  </Paragraph>
                  <Title level={3}>Response</Title>
                  <CCode mode="application/json" value={file_upload} />
                </Card>
                <Paragraph />
              </Col>
            </Row>

            <Row>
              <Col>
                <Card>
                  <Title level={2}>Receive a file</Title>
                  <Title level={3}>Request</Title>
                  <Paragraph code>
                    curl https://s.put.re/$id.png > /dev/null
                  </Paragraph>
                  <Title level={3}>Response</Title>
                  <CCode mode="text/plain" value={file_receive} />
                </Card>
                <Paragraph />
              </Col>
            </Row>

            <Row>
              <Col>
                <Card>
                  <Title level={2}>Delete a file</Title>
                  <Title level={3}>Request</Title>
                  <Paragraph code>
                    curl https://api.put.re/delete/fdmYuywt.png/uAfZhxyK
                  </Paragraph>
                  <Title level={3}>Response</Title>
                  <Paragraph>
                    <CCode mode="application/json" value={file_delete} />
                  </Paragraph>
                </Card>
                <Paragraph />
              </Col>
            </Row>

            <Row>
              <Col>
                <Card>
                  <Title level={2}>Request metadata of a file</Title>
                  <Title level={3}>Request</Title>
                  <Paragraph code>
                    curl https://api.put.re/metadata/fdmYuywt.png
                  </Paragraph>
                  <Title level={3}>Response</Title>
                  <Paragraph>
                    <CCode mode="application/json" value={file_metadata} />
                  </Paragraph>
                </Card>
              </Col>
            </Row>
          </Tabs.TabPane>

          <Tabs.TabPane key="2" tab="Albums">
            <Row>
              <Col>
                <Card>
                  <Title level={2}>Create an album</Title>
                  <Title level={3}>Request</Title>
                  <Paragraph code>
                    curl https://api.put.re/album/create
                  </Paragraph>
                  <Title level={3}>Response</Title>
                  <Paragraph>
                    <CCode mode="application/json" value={album_create} />
                  </Paragraph>
                </Card>
                <Paragraph />
              </Col>
            </Row>

            <Row>
              <Col>
                <Card>
                  <Title level={2}>Receive an album as JSON</Title>
                  <Title level={3}>Request</Title>
                  <Paragraph code>
                    curl https://api.put.re/album/$id.json
                  </Paragraph>
                  <Title level={3}>Response</Title>
                  <Paragraph>
                    <CCode mode="application/json" value={album_receive} />
                  </Paragraph>
                </Card>
                <Paragraph />
              </Col>
            </Row>

            <Row>
              <Col>
                <Card>
                  <Title level={2}>Receive an album as list</Title>
                  <Title level={3}>Request</Title>
                  <Paragraph code>
                    curl https://api.put.re/album/$id.txt
                  </Paragraph>
                  <Title level={3}>Response</Title>
                  <Paragraph>
                    <CCode
                      mode="text/plain"
                      value={"https://s.put.re/fdmYuywt.png"}
                    />
                  </Paragraph>
                </Card>
                <Paragraph />
              </Col>
            </Row>

            <Row>
              <Col>
                <Card>
                  <Title level={2}>Modify an album</Title>
                  <Title level={3}>Request</Title>
                  <Paragraph code>
                    curl -d "@album.json" -X POST
                    https://api.put.re/album/$id/modify
                  </Paragraph>
                  <Paragraph type="danger">
                    Post the whole album object and append the secret.
                  </Paragraph>
                  <Title level={3}>Response</Title>
                  <Paragraph>
                    <CCode mode="application/json" value={album_modify} />
                  </Paragraph>
                </Card>
                <Paragraph />
              </Col>
            </Row>

            <Row>
              <Col>
                <Card>
                  <Title level={2}>Delete an album</Title>
                  <Title level={3}>Request</Title>
                  <Paragraph code>
                    curl -d {`'{"secret":"xxxxxxxx"}'`} -H "Content-Type:
                    application/json" -X POST
                    https://api.put.re/album/$id/delete
                  </Paragraph>
                  <Title level={3}>Response</Title>
                  <Paragraph>
                    <CCode mode="application/json" value={album_delete} />
                  </Paragraph>
                </Card>
              </Col>
            </Row>
          </Tabs.TabPane>

          <Tabs.TabPane key="3" tab="Misc">
            <Row>
              <Col>
                <Card>
                  <Title level={2}>Check service health</Title>
                  <Title level={3}>Request</Title>
                  <Paragraph code>curl https://api.put.re/health</Paragraph>
                  <Title level={3}>Response</Title>
                  <Paragraph>
                    <CCode mode="application/json" value={misc_health} />
                  </Paragraph>
                </Card>
                <Paragraph />
              </Col>
            </Row>

            <Row>
              <Col>
                <Card>
                  <Title level={2}>Make a removal request</Title>
                  <Title level={3}>Request</Title>
                  <Paragraph>
                    <CCode mode="text/plain" value={misc_removal_req} />
                  </Paragraph>
                  <Title level={3}>Response</Title>
                  <Paragraph>
                    <CCode mode="application/json" value={misc_removal_res} />
                  </Paragraph>
                </Card>
              </Col>
            </Row>
          </Tabs.TabPane>
        </Tabs>
      </>
    );
  }
}
