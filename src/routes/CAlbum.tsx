import { Col, Divider, Empty, Row, Typography } from "antd";
import { inject, observer } from "mobx-react";
import React, { Component } from "react";
import { RouteComponentProps } from "react-router-dom";

import { IStores } from "../CApp";
import CCreated from "../partials/album/CCreated";
import CGridView from "../partials/album/CGridView";
import CLightView from "../partials/album/CLightView";
import CNavigation from "../partials/album/CNavigation";

interface CAlbumProps
  extends RouteComponentProps<{ albumId: string; secret: string }> {
  stores?: IStores;
}

@inject("stores")
@observer
export default class CAlbum extends Component<CAlbumProps> {
  private readonly store = this.props.stores!.album;

  componentDidMount() {
    const albumId = this.props.match.params.albumId;
    const secret = this.props.match.params.secret;

    if (albumId) {
      this.store.albumId = albumId;
    }

    if (secret) {
      this.store.secret = secret;
    }
  }

  private get bodyView(): JSX.Element {
    if (this.store.loaded && this.store.album.entries.length > 0) {
      return this.store.view === "light" ? <CLightView /> : <CGridView />;
    }

    return <Empty />;
  }

  private get title(): string {
    if (!this.store.loaded) {
      return "Your future album";
    }

    return this.store.album.title || "Untitled album";
  }

  render() {
    return (
      <>
        {this.store.isCreated && (
          <CCreated
            albumId={this.store.albumId}
            secret={this.store.secret}
            onClose={() => (this.store.isCreated = false)}
          />
        )}

        <Row>
          <Col>
            <Typography.Title
              editable={
                this.store.canEdit && {
                  editing: this.store.editing === `maint`,
                  onStart: () => (this.store.editing = `maint`),
                  onChange: value => this.store.setAlbumTitle(value)
                }
              }
            >
              {this.title}
            </Typography.Title>
            {this.store.loaded && (
              <Typography.Paragraph>
                hosts {this.store.album.entries.length} files; last updated{" "}
                {new Date(this.store.album.updatedAt).toLocaleString()}
              </Typography.Paragraph>
            )}
          </Col>
        </Row>

        <CNavigation />

        <Row>
          <Col>
            <Divider />
          </Col>
        </Row>

        <Row>
          <Col>{this.bodyView}</Col>
        </Row>
      </>
    );
  }
}
