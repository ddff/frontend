import { Button, Col, Result, Row } from "antd";
import React, { PureComponent } from "react";
import { RouteComponentProps } from "react-router";

export default class CNotFound extends PureComponent<RouteComponentProps> {
  render() {
    return (
      <Result
        extra={
          <Row justify="center" type="flex">
            <Col>
              <Button
                type="primary"
                onClick={() => this.props.history.goBack()}
              >
                Go back
              </Button>
            </Col>
            <Col style={{ padding: "0px 0px 0px 5px" }}>
              <Button
                type="primary"
                onClick={() => this.props.history.push("/")}
              >
                Go to index
              </Button>
            </Col>
          </Row>
        }
        status="404"
        subTitle="Sorry, the page you visited does not exist."
        title="404"
      />
    );
  }
}
