import { Col, Divider, List, message, Row, Spin, Typography } from "antd";
import axios, { AxiosResponse } from "axios";
import React, { Component } from "react";

interface CStatusProps {
  state?: CStatusState;
}

interface CStatusState {
  data?: APIStatusBody;
}

export default class CStatus extends Component<CStatusProps> {
  readonly state: CStatusState = {};

  async componentDidMount() {
    if (this.props.state !== undefined) {
      return this.setState(this.props.state);
    }

    try {
      const response: AxiosResponse<APIStatusResponse> = await axios.get(
        "https://api.put.re/health"
      );

      if (response.data.status === "success" && response.data.data) {
        this.setState({ data: response.data.data });
      }
    } catch (_) {
      message.error("Request failed! Please try again later.");
    }
  }

  private get isOK(): boolean {
    let flag = true;

    if (!this.state.data) {
      return false;
    }

    this.state.data.services.forEach(service => {
      if (service.status !== "OK") {
        flag = false;
      }
    });

    return flag;
  }

  render() {
    return (
      <>
        <Row>
          <Col>
            <Typography.Title>Status</Typography.Title>
          </Col>
        </Row>

        <Row>
          <Col>
            <Divider />
          </Col>
        </Row>

        {this.state.data ? (
          <>
            <Row>
              <Col>
                <List
                  bordered
                  dataSource={[
                    this.isOK ? "ALL SYSTEMS OPERATIONAL" : "SOME ISSUES"
                  ]}
                  renderItem={(item: string) => <List.Item>{item}</List.Item>}
                  style={{
                    backgroundColor: this.isOK ? "green" : "red",
                    color: "white",
                    fontWeight: "bold"
                  }}
                />
              </Col>
            </Row>

            <Row>
              <Col style={{ padding: "10px 0px 0px 0px" }}>
                <List
                  bordered
                  dataSource={this.state.data.services}
                  renderItem={item => (
                    <List.Item>
                      <Typography.Text code>[{item.status}]</Typography.Text>{" "}
                      {item.name} ({item.ping}ms)
                    </List.Item>
                  )}
                />
              </Col>
            </Row>

            <Row>
              <Col style={{ padding: "10px 0px 0px 0px" }}>
                <List
                  bordered
                  dataSource={["No past incidents."]}
                  renderItem={(item: string) => <List.Item>{item}</List.Item>}
                />
              </Col>
            </Row>
          </>
        ) : (
          <Spin size="large" />
        )}
      </>
    );
  }
}
