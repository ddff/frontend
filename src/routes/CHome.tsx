import { Alert, Button, Col, Divider, Row, Typography } from "antd";
import { inject, observer } from "mobx-react";
import React, { Component, lazy, Suspense } from "react";
import { Link } from "react-router-dom";

import { IStores } from "../CApp";
import CFileUploader from "../partials/home/CFileUploader";
import CPasteUploader from "../partials/home/CPasteUploader";
import CSettings from "../partials/home/CSettings";

const CFileList = lazy(() =>
  import(/* webpackPrefetch: true */ "../partials/home/CFileList")
);
const CTextUploader = lazy(() =>
  import(/* webpackPrefetch: true */ "../partials/home/CTextUploader")
);

// TODO Unload logic on different pages
require("magic-snowflakes")();
require("happy-holiday")(/*{ holiday: "halloween" }*/);

interface CHomeProps {
  stores?: IStores;
}

@inject("stores")
@observer
export default class CHome extends Component<CHomeProps> {
  private readonly store = this.props.stores!.file;

  render() {
    return (
      <>
        <CPasteUploader />

        {this.props.stores!.album.dirty && (
          <Row justify="center" type="flex">
            <Col>
              <Alert
                message={<Link to="/album">Remember to save your album</Link>}
                type="warning"
              />
            </Col>
          </Row>
        )}

        <Row justify="end" type="flex">
          <Col>
            <CSettings />
          </Col>
        </Row>

        <Row>
          <Col>
            {this.store.uploader === "file" ? (
              <CFileUploader />
            ) : (
              <Suspense fallback={null}>
                <CTextUploader />
              </Suspense>
            )}
          </Col>
        </Row>

        <Row justify="end" type="flex">
          <Col>
            <Button
              disabled={this.store.hasActiveUploads}
              style={{ fontWeight: "bold" }}
              type="link"
              onClick={() => this.store.toggleUploader()}
            >
              upload {this.store.uploader === "file" ? "text" : "files"}
            </Button>
          </Col>
        </Row>

        {this.store.files.length > 0 && (
          <>
            <Row>
              <Col>
                <Divider />
              </Col>
            </Row>

            <Suspense fallback={null}>
              <Row justify="center" type="flex">
                <Col>
                  <CFileList />
                </Col>
              </Row>
            </Suspense>

            <Row justify="end" type="flex">
              <Col>
                <Typography.Paragraph>
                  <Button size="large" onClick={() => this.store.clear()}>
                    Clear
                  </Button>
                </Typography.Paragraph>
              </Col>
            </Row>
          </>
        )}
      </>
    );
  }
}
