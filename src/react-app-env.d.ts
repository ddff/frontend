/// <reference types="react-scripts" />

interface JSendResponse<T> {
  data?: T;
  message?: string;
  status: "error" | "success";
}

interface APIAlbumCreateBody {
  id: string;
  secret: string;
}

type APIAlbumCreateResponse = JSendResponse<APIAlbumCreateBody>;

interface APIMetadataBody {
  mimeType: string;
  originalName: string;
  size: number;
}

type APIMetadataResponse = JSendResponse<APIMetadataBody>;

interface APIStatusBody {
  load: [number, number, number];
  services: { name: string; ping: number; status: string }[];
  uptime: number;
}

type APIStatusResponse = JSendResponse<APIStatusBody>;

interface APIUploadBody {
  deleteLink: string;
  deleteToken: string;
  extension: string;
  link: string;
  name: string;
  originalName: string;
  size: number;
}

type APIUploadResponse = JSendResponse<APIUploadBody>;

interface MyUploadFile {
  uid: string;
  size: number;
  name: string;
  fileName?: string;
  lastModified?: number;
  lastModifiedDate?: Date;
  url?: string;
  status?: "error" | "success" | "done" | "uploading" | "removed";
  percent?: number;
  thumbUrl?: string;
  originFileObj?: File | Blob;
  response?: APIUploadResponse;
  error?: any;
  linkProps?: any;
  type: string;
}
